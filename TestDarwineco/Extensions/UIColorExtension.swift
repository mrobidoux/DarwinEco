//
// Created by Mathieu Perso on 2016-08-10.
// Copyright (c) 2016 Mathieu Robidoux. All rights reserved.
//

/******* TAKEN FROM STACKOVERFLOW ********/
/* http://stackoverflow.com/questions/24074257/how-to-use-uicolorfromrgb-value-in-swift */

import Foundation
import UIKit

extension UIColor {
	convenience init(rgb: UInt) {
		self.init(
				red: CGFloat((rgb & 0xFF0000) >> 16) / 255.0,
				green: CGFloat((rgb & 0x00FF00) >> 8) / 255.0,
				blue: CGFloat(rgb & 0x0000FF) / 255.0,
				alpha: CGFloat(1.0)
				)
	}
}
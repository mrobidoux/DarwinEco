//
//  ResultViewController.swift
//  TestDarwineco
//
//  Created by Mathieu Perso on 2016-08-12.
//  Copyright © 2016 Mathieu Robidoux. All rights reserved.
//

import Foundation
import UIKit

class ResultViewController : UIViewController {

    var result : Result? {
        didSet {
            if let result = result {
                self.title = result.title
                self.scoreView.score = CGFloat(result.score)
                self.scoreView.average = CGFloat(result.average)
                self.scoreView.aboveAverageIsPositive = result.aboveAverageIsPositive
            }
        }
    }

    @IBOutlet weak var scoreView: ScoreView!

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override required init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
}
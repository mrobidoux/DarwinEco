//
//  ViewController.swift
//  TestDarwineco
//
//  Created by Mathieu Perso on 2016-08-10.
//  Copyright © 2016 Mathieu Robidoux. All rights reserved.
//

import UIKit
import PageMenu

class ViewController: UIViewController, CAPSPageMenuDelegate {

    @IBOutlet weak var indicatorView : IndicatorView!
    
	private var pagerOptions: [CAPSPageMenuOption] = [
			.MenuItemSeparatorWidth(4.3),
			.UseMenuLikeSegmentedControl(false),
			.MenuItemSeparatorPercentageHeight(0.1),
			.ViewBackgroundColor(UIColor.whiteColor()),
			.ScrollMenuBackgroundColor(UIColor.whiteColor()),
			.SelectionIndicatorColor(UIColor.whiteColor()),
			.UnselectedMenuItemLabelColor(UIColor.lightGrayColor()),
			.SelectedMenuItemLabelColor(UIColor(rgb: 0x666666)),
			.MenuItemWidthBasedOnTitleTextWidth(true),
			.MenuMargin(50),
			.MenuHeight(60),
			.CenterMenuItems(true)
	]

	private var pageMenu: CAPSPageMenu?

	override func viewDidLoad() {
		super.viewDidLoad()

		self.createPager()
	}

	override func preferredStatusBarStyle() -> UIStatusBarStyle {
		return .LightContent
	}

	func createPager() {

		let resultControllers = DataManager.results.map({
			result in
			let controller = NSBundle.mainBundle().loadNibNamed("ResultViewController", owner: nil, options: nil).first as! ResultViewController
			controller.result = result
			return controller
		}) as [UIViewController]

		self.pageMenu = CAPSPageMenu(viewControllers: resultControllers,
								frame: CGRectMake(0.0, 0.0, self.view.frame.width, self.view.frame.height),
								pageMenuOptions: pagerOptions)

		self.pageMenu?.delegate = self
		self.view.addSubview(pageMenu!.view)
		self.view.sendSubviewToBack(pageMenu!.view)
		self.indicatorView.results = DataManager.results
		self.indicatorView.index = 0
	}

	// MARK: CAPSPageMenuDelegate

	func willMoveToPage(controller: UIViewController, index: Int){
		if let controller = controller as? ResultViewController {
			controller.scoreView.resetAnimation()
		}
	}

	func didMoveToPage(controller: UIViewController, index: Int){
		if let controller = controller as? ResultViewController {
			controller.scoreView.startAnimation()
		}
		self.indicatorView.index = index
	}
}


//
//  ScoreView.swift
//  TestDarwineco
//
//  Created by Mathieu Perso on 2016-08-10.
//  Copyright © 2016 Mathieu Robidoux. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class ScoreView: UIView {

	/* Subviews */
	private let lblScore: UILabel = {
		let label = UILabel()
		label.textAlignment = .Center
		label.adjustsFontSizeToFitWidth = true
		return label
	}()

	private let lblAverage: UILabel = {
		let label = UILabel()
		label.textAlignment = .Left
		label.numberOfLines = 2
		label.adjustsFontSizeToFitWidth = true
		return label
	}()

	/* Layers */

	// This is the gradient that will be masked with a cercle
	private let backgroundLayer: CAGradientLayer = {
		let layer = CAGradientLayer()
		layer.startPoint = CGPointMake(0, 1)
		layer.endPoint = CGPointMake(1, 1)
		return layer
	}()

	// This is the layer for the full "background" circle
	private var backgroundCircleLayer: CAShapeLayer = {
		let layer = CAShapeLayer()
		layer.fillColor = UIColor.clearColor().CGColor
		layer.strokeColor = UIColor.blackColor().CGColor
		layer.lineWidth = 7.0
		layer.strokeEnd = 1.0
		return layer
	}()

	// This is the layer for the semi circle that will be animated
	private var semiCircleLayer: CAShapeLayer = {
		let layer = CAShapeLayer()
		layer.fillColor = UIColor.clearColor().CGColor
		layer.strokeColor = UIColor.blackColor().CGColor
		layer.lineWidth = 7.0
		layer.strokeEnd = 0.0
		return layer
	}()

	// This is the layer for the average circle
	private var averageCircleLayer: CAShapeLayer = {
		let layer = CAShapeLayer()
		layer.fillColor = UIColor.whiteColor().CGColor
		layer.lineWidth = 2.0
		return layer
	}()

	/* Paths */

	//This is the path for the semi circle and full circle
	private var semiCirclePath: UIBezierPath? {
		didSet {
			self.semiCircleLayer.path = self.semiCirclePath?.CGPath
			self.backgroundCircleLayer.path = self.semiCirclePath?.CGPath
		}
	}

	//This is the path for the average circle
	private var averageCirclePath: UIBezierPath? {
		didSet {
			self.averageCircleLayer.path = self.averageCirclePath?.CGPath
		}
	}

	/* Animations */
	private let semiCircleAnimation: CABasicAnimation = {
		let animation = CABasicAnimation(keyPath: "strokeEnd")
		animation.fromValue = 0
		animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
		return animation
	}()

	/* Properties */
	private var circleRadius: CGFloat {
		return (self.frame.size.width - (self.lineWidth * 2)) / 2
	}

	private var averageCircleRadius: CGFloat {
		return self.frame.size.width * 0.05
	}

	private var scoreString: String {
		return String(format: "%.0f", self.score * 100)
	}

	private var averageString: String {
		return String(format: "Average\n%.0f%%", self.average * 100)
	}

	//This is the duration of a full 100% animation. A score 50% will take half that time
	@IBInspectable
	var animationDuration: CGFloat = 2 {
		didSet {
			self.updateDuration()
		}
	}

	// Colors are redefined here in case we don't want to use the default ones
	@IBInspectable
	var colorsPositive: [UIColor] = AppConstants.Colors.positive
	@IBInspectable
	var colorsNegative: [UIColor] = AppConstants.Colors.negative

	@IBInspectable
	var score: CGFloat = 0.0 {
		didSet {
			self.semiCircleAnimation.toValue = self.score
			self.semiCircleLayer.strokeEnd = self.score
			self.lblScore.text = self.scoreString
			self.updateDuration()
		}
	}

	@IBInspectable
	var average: CGFloat = 0.0 {
		didSet {
			self.updateColors()
			self.updateAverageCircleAndText()
		}
	}

	// Some times, higher is not better
	@IBInspectable
	var aboveAverageIsPositive: Bool = true {
		didSet {
			self.updateColors()
		}
	}

	@IBInspectable
	var lineWidth: CGFloat = 7 {
		didSet {
			self.backgroundCircleLayer.lineWidth = self.lineWidth
			self.semiCircleLayer.lineWidth = self.lineWidth
		}
	}

	override var frame: CGRect {
		didSet {
			self.backgroundLayer.frame = self.bounds
			self.semiCirclePath = UIBezierPath(
					arcCenter: CGPoint(x: self.frame.size.width / 2.0, y: self.frame.size.height / 2.0)
					, radius: self.circleRadius
					, startAngle: CGFloat(toRadians(270.001))
					, endAngle: CGFloat(toRadians(270))
					, clockwise: true)



			self.lblScore.frame = self.bounds
			self.lblScore.font = UIFont.systemFontOfSize(self.frame.size.width / 2)
			self.lblAverage.font = UIFont.systemFontOfSize(self.frame.size.width / 18)

			updateAverageCircleAndText()
		}
	}

	override func awakeFromNib() {
		super.awakeFromNib()

		// Adding the different components
		self.backgroundLayer.mask = semiCircleLayer
		self.layer.addSublayer(backgroundCircleLayer)
		self.layer.addSublayer(backgroundLayer)
		self.layer.addSublayer(averageCircleLayer)

		self.addSubview(self.lblScore)
		self.addSubview(self.lblAverage)

		// Initializing default values
		self.updateColors()
		self.updateDuration()
	}

	func startAnimation() {
		semiCircleLayer.addAnimation(self.semiCircleAnimation, forKey: "circleAnimation")
		semiCircleLayer.hidden = false
	}

	func resetAnimation() {
		semiCircleLayer.hidden = true
	}

	private func updateAverageCircleAndText() {
		// x = cx + r * cos(a)
		// y = cy + r * sin(a)

		let angle = toRadians(360 * self.average - 90) // -90 because we start at 12h
		let x     = CGFloat(cos(angle)) * self.circleRadius + (self.frame.width / 2)
		let y     = CGFloat(sin(angle)) * self.circleRadius + (self.frame.height / 2)

		self.averageCirclePath = UIBezierPath(
				arcCenter: CGPoint(x: x, y: y)
				, radius: self.averageCircleRadius
				, startAngle: CGFloat(0)
				, endAngle: CGFloat(M_PI * 2)
				, clockwise: true)

		self.lblAverage.text = self.averageString

		//Measure the size of the string
		let textSize = NSString(string: self.averageString).sizeWithAttributes([NSFontAttributeName: self.lblAverage.font])

		var averageLabelX : CGFloat = 0;
		var averageLabelY : CGFloat = 0;

		//Check in which corner we should place the label so it dosen't go over the circle
		switch self.average {
			case 0 ..< 0.25:
				averageLabelX = x + self.averageCircleRadius
				averageLabelY = y - self.averageCircleRadius - textSize.height
			case 0.25 ..< 0.5:
				averageLabelX = x + self.averageCircleRadius
				averageLabelY = y + self.averageCircleRadius
			case 0.5 ..< 0.75:
				averageLabelX = x - self.averageCircleRadius - textSize.width
				averageLabelY = y + self.averageCircleRadius
			default: // > 0.75
				averageLabelX = x - self.averageCircleRadius - textSize.width
				averageLabelY = y - self.averageCircleRadius - textSize.height
		}


		self.lblAverage.frame = CGRectMake(averageLabelX, averageLabelY, textSize.width, textSize.height)
	}

	private func updateColors() {

		//Choose what colors to use
		let colors = (score >= average) == aboveAverageIsPositive ? self.colorsPositive : self.colorsNegative

		self.backgroundLayer.colors = colors.map({ $0.CGColor }) // UIColors need to be converted to CGColors
		self.backgroundCircleLayer.strokeColor = colors.first?.colorWithAlphaComponent(0.1).CGColor //Take the first color with an alpha of 10%
		self.lblScore.textColor = colors.first // No need to convert to CGColors here
		self.lblAverage.textColor = colors.first // No need to convert to CGColors here
		self.averageCircleLayer.strokeColor = colors.first?.CGColor
	}

	private func updateDuration() {
		// Make sure the animation will always go at the same speed
		self.semiCircleAnimation.duration = Double(self.score * self.animationDuration)
	}
}
//
//  Indicator.swift
//  TestDarwineco
//
//  Created by Mathieu Perso on 2016-08-12.
//  Copyright © 2016 Mathieu Robidoux. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class IndicatorView: UIView {

	@IBInspectable
	var indicatorWidth: CGFloat = 5

	@IBInspectable
	var indicatorMargin: CGFloat   = 16

	// Colors are redefined here in case we don't want to use the default ones
	@IBInspectable
	var colorsPositive:  [UIColor] = AppConstants.Colors.positive
	@IBInspectable
	var colorsNegative:  [UIColor] = AppConstants.Colors.negative

	// Colors of the dots. Only used here so no need to put in AppConstants
	@IBInspectable
	var dotNeutral                 = UIColor(rgb: 0xdbdbdb).CGColor
	//Colors need to be converted from UIColor to CGColor for drawing
	@IBInspectable
	var dotActive                  = UIColor(rgb: 0xadadad).CGColor
	//Colors need to be converted from UIColor to CGColor for drawing

	var results: [Result] = [] {
		didSet {
			self.setNeedsDisplay()
		}
	}

	var index: Int? {
		didSet {
			self.setNeedsDisplay()
		}
	}

	override required init(frame: CGRect) {
		super.init(frame: frame)
	}

	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}

	override func drawRect(rect: CGRect) {
		super.drawRect(rect)

		//Make sure we are clean
		self.layer.sublayers = nil

		//Convert from Int to CGFloat to be able to do calculations with that value
		let nbResults                    = CGFloat(self.results.count)

		//The color lines should take a maximum of 80% of the height of the view
		let fullIndicatorHeight: CGFloat = rect.height * 0.8

		//Total width of the indicators
		let totalWidth                   = self.indicatorWidth * nbResults + self.indicatorMargin * (nbResults - 1)

		//The offset on the x axis to center everything
		let offsetX                      = rect.width / 2 - totalWidth / 2

		for (index, result) in results.enumerate() {
			let colors = result.isPositive ? self.colorsPositive : self.colorsNegative

			//Convert from Int to CGFloat to be able to do calculations with that value
			let i      = CGFloat(index)
			let score  = CGFloat(result.score)

			let x = self.indicatorWidth * i + self.indicatorMargin * i + offsetX
			let h = fullIndicatorHeight * score
			let y = fullIndicatorHeight - h //Align all the lines even if they don't have the same height

			/* Create the idicator lines */

			let indicatorLayer = CAGradientLayer()
			indicatorLayer.startPoint = CGPointMake(0, 0) //Gradient from top to bottom
			indicatorLayer.endPoint = CGPointMake(0, 1)
			indicatorLayer.colors = colors.map({ $0.CGColor }) //Colors need to be converted from UIColor to CGColor for drawing
			indicatorLayer.frame = CGRectMake(x, y, self.indicatorWidth, h)

			let indicatorMask = CAShapeLayer()
			indicatorMask.fillColor = UIColor.blackColor().CGColor
			indicatorMask.path = UIBezierPath(roundedRect: CGRectMake(0, 0, self.indicatorWidth, h),
											  cornerRadius: self.indicatorWidth).CGPath

			indicatorLayer.mask = indicatorMask

			/* Create the dots under the lines */

			let dotX      = x + self.indicatorWidth / 2
			let dotY      = rect.height - rect.height * 0.07 // 70% for the big dot
			let dotRadius = rect.height * (self.index == index ? 0.07 : 0.05) // 50% and 70% give a good result

			let dotLayer = CAShapeLayer()
			dotLayer.fillColor = self.index == index ? self.dotActive : self.dotNeutral
			dotLayer.path = UIBezierPath(arcCenter: CGPoint(x: dotX, y: dotY),
										 radius: dotRadius,
										 startAngle: 0,
										 endAngle: CGFloat(M_PI * 2),
										 clockwise: true).CGPath


			self.layer.addSublayer(indicatorLayer)
			self.layer.addSublayer(dotLayer)
		}
	}
}

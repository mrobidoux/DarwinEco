//
// Created by Mathieu Perso on 2016-08-12.
// Copyright (c) 2016 Mathieu Robidoux. All rights reserved.
//

import Foundation

class Result {

	var title : String
	var description : String
	var score : Float
	var average : Float
	var aboveAverageIsPositive : Bool

	var isPositive : Bool {
		return (score >= average) == aboveAverageIsPositive
	}

	init(title: String, description : String, score: Float, average: Float, aboveAverageIsPositive: Bool = true){
		self.title = title
		self.description = description
		self.score = score
		self.average = average
		self.aboveAverageIsPositive = aboveAverageIsPositive
	}

}
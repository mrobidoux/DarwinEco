//
// Created by Mathieu Perso on 2016-08-12.
// Copyright (c) 2016 Mathieu Robidoux. All rights reserved.
//

import Foundation

//This is where application data is saved
class DataManager {

	static var results = [Result]()

	class func getResultsFromSomewhere(){

		results.append(Result(
				title: "Openness",
				description: "",
				score: 0.67,
				average: 0.3
				))

		results.append(Result(
				title: "Conscientiousness",
				description: "",
				score: 0.45,
				average: 0.89
				))

		results.append(Result(
				title: "Extraversion",
				description: "",
				score: 0.56,
				average: 0.25
				))

		results.append(Result(
				title: "Agreeableness",
				description: "",
				score: 0.89,
				average: 0.27
				))
	}
}
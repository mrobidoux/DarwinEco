//
// Created by Mathieu Perso on 2016-08-12.
// Copyright (c) 2016 Mathieu Robidoux. All rights reserved.
//

import Foundation
import UIKit

class AppConstants {

	class Colors {
		static let positive = [UIColor(rgb: 0x30aff9), UIColor(rgb: 0x5885ff)]
		static let negative = [UIColor(rgb: 0xf26661), UIColor(rgb: 0xff9451)]
	}
}
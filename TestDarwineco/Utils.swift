//
// Created by Mathieu Perso on 2016-08-12.
// Copyright (c) 2016 Mathieu Robidoux. All rights reserved.
//

import Foundation
import UIKit

func toRadians(degress : Double) -> Double{
	return degress * M_PI / 180
}

func toRadians(degress : CGFloat) -> CGFloat{
	return degress * CGFloat(M_PI) / 180
}